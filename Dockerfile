FROM nvcr.io/nvidia/cuda:12.6.0-base-ubuntu22.04
LABEL maintainer="kdietz"
COPY .    /app/
WORKDIR /app

RUN apt-get update -y 

# Install Curl and Git
RUN apt-get -y install curl && apt-get install -y git

# Install Python and Pip
RUN apt-get install -y python3.11
RUN apt-get install -y python3-pip 

# Install UV
RUN curl -LsSf https://astral.sh/uv/install.sh | sh

# Install Python Packages via UV
RUN /root/.local/bin/uv sync --all-groups --reinstall 

