## Data Team

The [handbook](https://handbook.gitlab.com/handbook/enterprise-data/) is the single source of truth for all of our documentation. 
Please see the [Jupyter Guide](https://handbook.gitlab.com/handbook/enterprise-data/platform/jupyter-guide/) for info on how to get started.  

### Contributing

We welcome contributions and improvements, please see the [contribution guidelines](CONTRIBUTING.md).

### License

This code is distributed under the MIT license, please see the [LICENSE](LICENSE) file.
