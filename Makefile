.PHONY: build

UNAME := $(shell uname -m)

.EXPORT_ALL_VARIABLES:
SNOWFLAKE_SNAPSHOT_DATABASE=SNOWFLAKE
SNOWFLAKE_LOAD_DATABASE=RAW
SNOWFLAKE_PREP_DATABASE=PREP
SNOWFLAKE_PREP_SCHEMA=preparation
SNOWFLAKE_PROD_DATABASE=PROD
SNOWFLAKE_TRANSFORM_WAREHOUSE=ANALYST_XS

.DEFAULT: help
help:
	@echo "\n \
	------------------------------ \n \
 	\n \
	++ Jupyter Related ++ \n \
	setup-jupyter-local: Set up a local instance of jupyter lab on your local machine in a viritual environment \n \
	jupyter-local: Spins up a jupyter instance from a virtual environment hosted at http://127.0.0.1:8888 \n \
	jupyter-docker: Spins up a jupyter instance from a docker image hosted at http://127.0.0.1:8888 \n \
	vscode-local: Spins up a jupyter instance from a virtual environment hosted at http://127.0.0.1:8888. Does not launch jupyter lab. Instead launches VS Code \n \
	\n \
	++ Utilities ++ \n \
	uv-recompile: re-compiles pyproject.toml when new dependencies or versions are added/removed. \n \
	------------------------------ \n"
	
uv-recompile:
	@echo "Re-Compiling and generating a new pyproject.toml"
	@uv remove pandas numpy ipykernel gitlabdata gitlabds numba papermill pyodbc scikit-learn scipy snowflake-connector-python snowflake-sqlalchemy sympy xgboost black fastdist feast 
	@uv remove autots interpret kmodes keras tensorflow lifelines mlflow optuna optuna-integration plotly pyqt6 shap ydata-profiling --group dev
	@uv remove ipywidgets jupyterlab jupyterlab-code-formatter jupyterlab-execute-time jupyterlab-git jupyterlab-templates line-profiler memory-profiler --group jupyter

	@uv add pandas numpy ipykernel gitlabdata gitlabds numba papermill pyodbc "scikit-learn<1.6" "scipy>=1.13.1" snowflake-connector-python snowflake-sqlalchemy sympy xgboost black fastdist feast 
	@uv add autots interpret kmodes keras tensorflow lifelines mlflow optuna optuna-integration plotly pyqt6 shap ydata-profiling --group dev
	@uv add ipywidgets jupyterlab jupyterlab-code-formatter jupyterlab-execute-time jupyterlab-git jupyterlab-templates line-profiler memory-profiler --group jupyter


setup-jupyter-local:
	@echo "Setting up Jupyter using virtual environment"
	@echo "Installing uv"
	@curl -LsSf https://astral.sh/uv/install.sh | sh 
	@echo "Ensure SSL certs are up to date"
	@pip install --upgrade certifi
	@uv sync --all-groups --reinstall  
	@./.venv/bin/jupyter lab build
	@echo "Data Science environment successfully created"

jupyter-local:
	@echo "Running local Jupyter"
	@./.venv/bin/jupyter lab

jupyter-docker:
	@docker-compose down
	@docker system prune -f
	@docker-compose up

vscode-local:
	@echo "Running Jupyter via VS Code"
	@code
	@./.venv/bin/jupyter lab --no-browser

